import warnings
import json
warnings.filterwarnings("ignore")
import sys

from dejavu import Dejavu
from dejavu.recognize import FileRecognizer, MicrophoneRecognizer
import argparse

if len(sys.argv) < 2:
    print("Analyse a sound file.\n\nUsage: %s filename.mp3" % sys.argv[0])
    sys.exit(-1)


# load config from a JSON file (or anything outputting a python dictionary)
with open("dejavu.cnf.SAMPLE") as f:
    config = json.load(f)

if __name__ == '__main__':

	# create a Dejavu instance
	djv = Dejavu(config)

	# Fingerprint all the mp3's in the directory we give it
	#djv.fingerprint_directory("mp3", [".mp3"])

	# Recognize audio from a file
	song = djv.recognize(FileRecognizer, sys.argv[1])
	print "From file we recognized: %s\n" % song
